@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="{{ url('/employee-add') }}">Add Employee </a> </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
<table>
     @if(count($employee))  
  <tr>
    <th>Name</th>
    <th>Company</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Action</th>
  </tr>
    @foreach($employee as $emp)
  <tr>

    <td>@if($emp->first_name) {{$emp->first_name}} @else --- @endif  @if($emp->last_name) {{$emp->last_name}} @else --- @endif </td>
    <td>@if($emp->company) {{App\Company::where('id',$emp->company)->first()->name}} @else --- @endif</td>
    <td>@if($emp->email) {{$emp->email}} @else --- @endif</td>
    <td>@if($emp->phone) {{$emp->phone}} @else --- @endif</td>
    <td><a href="{{ url('/employee-delete') }}/{{$emp->id}}"> <i class="fa fa-trash-o"></i> </a></td>
  </tr>
  @endforeach
    {{ $employee->links() }}
  @else
  No data found

  @endif

 
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>