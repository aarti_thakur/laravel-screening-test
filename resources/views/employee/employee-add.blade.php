@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="{{ url('/employee') }}"> <- List Employee </a> </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ url('/employee-store') }}" method="get">
                      <input type="hidden" name="_token" value="{{csrf_token()}}">
                  <label for="fname">First name:</label><br>
                  <input class="form-control" type="text" id="fname" name="fname" ><br>
                  <label for="lname">Last name:</label><br>
                  <input class="form-control" type="text" id="lname" name="lname"><br>
                   <label for="company">Company:</label><br>
                  <select class="form-control">
                    @if($company)
                    @foreach($company as $cmp)
                    <option value="{{$cmp->id}}">{{$cmp->name}}</option>
                    @endforeach
                    @endif
                  </select><br>
                   <label for="email">Email:</label><br>
                  <input class="form-control" type="email" id="email" name="email" ><br>
                   <label for="phone">Phone:</label><br>
                  <input class="form-control" type="text" id="phone" name="phone"><br>
                  <input type="submit" value="Submit">
                </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>