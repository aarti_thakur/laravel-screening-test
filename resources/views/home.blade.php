@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
<div class="row">
    <div class="col-md-6"><div class="card">
          <a href="{{ url('/employee') }}">
  <img src=" {{ url('/images') }}/company.png" alt="Avatar" style="width:100%">
</a>
  <div class="container">
    <p>Company</p> 
  </div>
</div></div> <div class="col-md-6"><div class="card">
    <a href="{{ url('/employee') }}">
  <img src="{{ url('/images') }}/Employee.png" alt="Avatar" style="width:100%"></a>
  <div class="container">
   <p>Employee</p> 
  </div>
</div></div>
</div>
               
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 40%;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
  padding: 2px 16px;
}
</style>