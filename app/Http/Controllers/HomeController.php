<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function ShowEmployee()
    {
       $data['employee'] = Employee::paginate(10);
       return view('employee.employee-show',$data);

    }
    public function AddEmployee()
    {
      $data['company']=Company::all();
       return view('employee.employee-add',$data);

    }
    public function StoreEmployee(Request $request)
    {
        $validatedData = $request->validate([
    'fname' => ['required'],
    'lname' => ['required'],
        ]);
    $emp = new Employee;
    $emp->first_name = $request->fname;
    $emp->last_name = $request->lname;
    $emp->company = $request->company;
    $emp->email = $request->email;
    $emp->phone = $request->phone;
    $emp->save();

      
       return redirect(url('/employee'));

    }
    public function DeleteEmployee($id)
    {
      Employee::where('id',$id)->delete();
       return redirect()->back();

    }
}
